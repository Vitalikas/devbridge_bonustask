package lt.vitalij.kolinko.devbridge.exercise.util;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class DateUtil {
    public static void printBonusDatesBetween(int fromYear, int toYear) {
        int fromYear_ = fromYear;
        Logger logger = Logger.getLogger("log");
        try {
            FileHandler fh = new FileHandler("errors.log", true);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (IOException e) {
            System.err.println("Log file not found");
        }

        try {
            int count = 0;
            if ((fromYear > 0) && (fromYear < toYear) && (toYear < 10000)) {
                while (fromYear < toYear) {
                    String firstTwoDigits = new DecimalFormat("00").format(fromYear / 100);
                    String lastTwoDigits = new DecimalFormat("00").format(fromYear % 100);

                    String month;
                    String day;
                    String bonusDates;

                    month = reverse(lastTwoDigits);
                    day = reverse(firstTwoDigits);

                    int monthToInt = Integer.parseInt(month);
                    int dayToInt = Integer.parseInt(day);

                    if (dayToInt > 0) {
                        if ((monthToInt == 1 && dayToInt <= 31) ||
                                (monthToInt == 2 && dayToInt <= 29 && isLeap(fromYear)) ||
                                (monthToInt == 2 && dayToInt <= 28 && !isLeap(fromYear)) ||
                                (monthToInt == 3 && dayToInt <= 31) ||
                                (monthToInt == 4 && dayToInt <= 30) ||
                                (monthToInt == 5 && dayToInt <= 31) ||
                                (monthToInt == 6 && dayToInt <= 30) ||
                                (monthToInt == 7 && dayToInt <= 31) ||
                                (monthToInt == 8 && dayToInt <= 31) ||
                                (monthToInt == 9 && dayToInt <= 30) ||
                                (monthToInt == 10 && dayToInt <= 31) ||
                                (monthToInt == 11 && dayToInt <= 30) ||
                                (monthToInt == 12 && dayToInt <= 31)) {
                            bonusDates = String.format("%s-%s-%s", fromYear, month, day);
                            System.out.println(bonusDates);
                            count++;
                        }
                    }
                    fromYear++;
                }
//                System.out.printf("Total bonus dates from %d to %d is: %d", fromYear_, toYear, count);
                if (count == 0) {
                    System.out.println("There are no bonus dates for given interval");
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, "Incorrect date interval!\n" +
                    "Started from: " + fromYear_ + "\nEnded with: " + toYear, e);
            logger.log(Level.INFO, "Please edit the interval to continue!");
            System.out.print("Enter correct date to start from: ");
            IoUtil ioUtil = new IoUtil();
            fromYear = ioUtil.validateInput();
            System.out.print("Enter correct date to end with: ");
            toYear = ioUtil.validateInput();
            printBonusDatesBetween(fromYear, toYear);
        }
    }

    private static String reverse(String n) {
        StringBuilder output = new StringBuilder();
        for (int i = n.length()-1; i >= 0; i--) {
            output.append(n.charAt(i));
        }
        return String.valueOf(output);
    }

    private static boolean isLeap(int year) {
        boolean leap;
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    leap = true;
                } else {
                    leap = false;
                }
            } else {
                leap = true;
            }
        } else {
            leap = false;
        }
        return leap;
    }
}
