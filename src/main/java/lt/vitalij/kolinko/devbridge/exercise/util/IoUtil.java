package lt.vitalij.kolinko.devbridge.exercise.util;

import java.util.Scanner;

public class IoUtil {
    private final Scanner scanner;

    public IoUtil() {
        this.scanner = new Scanner(System.in);
    }

    public int validateInput() {
        int number;
        do {
            while (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.println(input + " is not a valid number! Please try again");
            }
            number = scanner.nextInt();
            if (number <= 0) {
                System.out.println("Year must be positive! Please try again");
            }
        } while (number <= 0);
        return number;
    }
}
