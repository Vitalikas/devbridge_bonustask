# Bonus dates
Bonus dates is implementation of Java method that prints all dates between two given years that remain the same 
if numbers of the date are reversed.<br />
It prints dates of interval from fromYear (inclusive) to toYear (exclusive) that satisfy the condition.
 
 For example, calling _printBonusDatesBetween(2010, 2015)_ it prints:<br />
 `2010-01-02`<br />
 `2011-11-02`
 
 ## Usage
 This is a [Maven](https://maven.apache.org/) project with console UI
 
 ## How to run
 1. Clone or download project
 2. Run Main.java
 
 ## Visuals
 Using IntelliJ IDEA:
 ![pic](demo.png)
 
 ## Extra functionalities
 - Error logging caused by an incorrect interval provided in method parameters
 - Ability to edit interval in console after throwing an exception
 - Input validator
 
 ## Others
 Script works with date interval: 1-9999